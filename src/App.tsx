import { useEffect, useState } from "react";
import "./App.css";
import useTypeWriterEffect from "./useTypewriterEffect";

export default function App() {
  const [flag, setFlag] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const text = useTypeWriterEffect(flag, 500);

  useEffect(() => {
    // Mocks url with a text file
    const url: string = "./sourceToFetch.txt";
    try {
      // Uses browser API and the setFlag as an array in order to showcase more complex use case
      fetch(url)
        .then((response) => response.text())
        .then((result) => {
          setFlag(Array.from(result));
        });
    } catch (error) {
      if (error instanceof Error) console.error(error.message);
    } finally {
      setIsLoading(false);
    }
  }, []);

  return (
    <div className="App">
      {isLoading ? (
        <>Loading...</>
      ) : (
        <>
          <h1>
            Typewriter effect via custom useTypeWriterEffect hook is presented
            below
          </h1>
          <h2>{text}</h2>
        </>
      )}
    </div>
  );
}
