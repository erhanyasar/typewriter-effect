import { useEffect, useRef, useState } from "react";

export default function useTypeWriterEffect(
  textList: string[],
  keyStrokeDuration: number
): string {
  const [currentPosition, setCurrentPosition] = useState<number>(0);
  const currentPositionRef = useRef<number>(0);

  useEffect(() => {
    /*
     * Uses setInterval on mount and makes use of both ref and state
     * variables in order to keep only one of them in dependency array
     */
    const intervalID = setInterval(() => {
      setCurrentPosition((value) => value + 1);
      currentPositionRef.current += 1;
      // In order to stop propagation after textList length
      if (currentPositionRef.current > textList.length)
        clearInterval(intervalID);
    }, keyStrokeDuration);

    return () => {
      // Uses clearInterval and reset methods upon unmount
      clearInterval(intervalID);
      currentPositionRef.current = 0;
      setCurrentPosition(0);
    };
  }, [keyStrokeDuration, textList]);
  // returns each letter as a substring from a list (array)
  return textList.join("").substring(0, currentPosition);
}
